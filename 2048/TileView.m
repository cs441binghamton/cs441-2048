//
//  TileView.m
//  2048
//
//  Created by Patrick Madden on 2/5/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import "TileView.h"

@implementation TileView
@synthesize label;
@synthesize value;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self addSubview:label];
        [label setText:@"---"];
        [label setTextAlignment:NSTextAlignmentCenter];
        [self setBackgroundColor:[UIColor redColor]];
    }
    return self;
}

-(void)updateLabel
{
    if (value == 0)
        [label setText:@""];
    else
        [label setText:[NSString stringWithFormat:@"%d", value]];
}
@end
