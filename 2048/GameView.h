//
//  GameView.h
//  2048
//
//  Created by Patrick Madden on 2/5/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TileView.h"

@interface GameView : UIView
@property (nonatomic, strong) NSArray *directions;
@property (nonatomic, strong) NSMutableArray *tiles;

@end
