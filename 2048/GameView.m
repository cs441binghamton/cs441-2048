//
//  GameView.m
//  2048
//
//  Created by Patrick Madden on 2/5/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import "GameView.h"

@implementation GameView
@synthesize directions;
@synthesize tiles;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(int)value:(int)direction row:(int)row col:(int)col
{
    NSArray *a = [directions objectAtIndex:direction];
    a = [a objectAtIndex:row];
    NSLog(@"Array A has %d elements", (int)[a count]);
    
    for (NSNumber *b in a)
    {
        NSLog(@"  Contains %@", b);

    }
    NSNumber *b = [a objectAtIndex:col];
    
    return (int)[b integerValue];
}

// To make life easier, I'm creating arrays with the indices
// for each of the four possible compression directions.
//
// There's north, south, east, west.  The tile spots are
// numbered 0  1  2  3
//          4  5  6  7
//          8  9 10 11
//         12 13 14 15

-(TileView *)findEmptyTile
{
    int i = rand() % 16;
    for (int j = 0; j < 15; ++j)
    {
        TileView *tv = [tiles objectAtIndex:(i + j)%16];
        if ([tv value] == 0)
            return tv;
    }
    
    return nil;
}


-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        tiles = [[NSMutableArray alloc] init];
        CGRect bounds = [self bounds];
        float w = bounds.size.width/4;
        float h = bounds.size.height/4;
        for (int r = 0; r < 4; ++r)
        {
            for (int c = 0; c < 4; ++c)
            {
                TileView *tv = [[TileView alloc] initWithFrame:CGRectMake(c*w + 4, r*h + 4, w - 8, h - 8)];
                [tiles addObject:tv];
                [tv setValue:0];
                [self addSubview:tv];
            }
        }
        
        NSArray *north = @[@[@( 0), @( 4), @( 8), @(12)],
                           @[@( 1), @( 5), @( 9), @(13)],
                           @[@( 2), @( 6), @(10), @(14)],
                           @[@( 3), @( 7), @(11), @(15)]];
        NSArray *south = @[@[@(12), @( 8), @( 4), @( 0)],
                           @[@(13), @( 9), @( 5), @( 1)],
                           @[@(14), @(10), @( 6), @( 2)],
                           @[@(15), @(11), @( 7), @( 3)]];
        NSArray *east  = @[@[@( 3), @( 2), @( 1), @( 0)],
                           @[@( 7), @( 6), @( 5), @( 4)],
                           @[@(11), @(10), @( 9), @( 8)],
                           @[@(15), @(14), @(13), @(12)]];
        NSArray *west  = @[@[@( 0), @( 1), @( 2), @( 3)],
                           @[@( 4), @( 5), @( 6), @( 7)],
                           @[@( 8), @( 9), @(10), @(11)],
                           @[@(12), @(13), @(14), @(15)]];
        
        
        directions = @[north, south, east, west];
    }
    return self;
}

-(IBAction)resetGame:(id)sender
{
    for (int i = 0; i < 16; ++i)
    {
        TileView *tv = [tiles objectAtIndex:i];
        [tv setValue:0];
        [tv updateLabel];
    }
    for (int i = 0; i < 3; ++i)
    {
        TileView *tv = [self findEmptyTile];
        if (tv)
        {
            [tv setValue:2];
            [tv updateLabel];
        }
    }
}

// Compression in a given direction.  The
// button that triggers the call to this method should
// have a Tag set of 0, 1, 2, or 3, for each of the
// different directions.
//
// We use the tile indexing from the directions array,
// and then go through each of the four rows, inserting
// the numbers as needed.
// 
-(IBAction)compress:(id)sender
{
    int newValues[4];
    int merged[4];
    
    NSArray *compression = [directions objectAtIndex:[sender tag]];
    for (int row = 0; row < 4; ++row)
    {
        NSLog(@"Extract row %d", row);
        // Get the indices of the spots we want to compress
        NSArray *thisRow = [compression objectAtIndex:row];

        for (int i = 0; i < 4; ++i)
        {
            newValues[i] = 0;
            merged[i] = 0;
        }
        int index = 0;
        
        for (int i = 0; i < 4; ++i)
        {
            NSNumber *n = [thisRow objectAtIndex:i];
            TileView *tv = [tiles objectAtIndex:[n integerValue]];
            NSLog(@"Tile %d has value %d", i, [tv value]);
            if ([tv value] > 0)
            {
                if (index == 0)
                {
                    NSLog(@"Insert %d", [tv value]);
                    newValues[index++] = [tv value];
                }
                else
                {
                    if ((newValues[index - 1] == [tv value]) && (!merged[index - 1]))
                    {
                        NSLog(@"Merge to %d", [tv value] * 2);
                        newValues[index - 1] = newValues[index - 1] * 2;
                        merged[index - 1] = 1;
                    }
                    else
                    {
                        NSLog(@"Insert %d", [tv value]);
                        newValues[index++] = [tv value];
                    }
                }
            }
        }
        
        // Fix up the labels!
        for (int i = 0; i < 4; ++i)
        {
            NSNumber *n = [thisRow objectAtIndex:i];
            TileView *tv = [tiles objectAtIndex:[n integerValue]];
            [tv setValue:newValues[i]];
            [tv updateLabel];
        }
    }
    
    TileView *tv = [self findEmptyTile];
    if (tv)
    {
        [tv setValue:2];
        [tv updateLabel];
    }
    else
    {
        NSLog(@"No empty spot!");
    }
}

@end
